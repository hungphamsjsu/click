package com.yomemo.clicklistenerexample;

public class Movie {
    public String id;
    public String title;
    public int image;

    public Movie(String id, String title, int image) {
        this.id = id;
        this.title = title;
        this.image = image;
    }
}
