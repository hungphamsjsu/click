package com.yomemo.clicklistenerexample;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yomemo.clicklistenerexample.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        List<Movie> data = getMovies();
        adapter = new MovieAdapter(data);
        adapter.setOnClickListener(this::onItemClick);
        binding.recyclerView.setAdapter(adapter);
    }

    public void onItemClick(int position) {
        Movie movie = adapter.getMovie(position);
        boolean isLastItem = adapter.isLastItem(position);
        Toast.makeText(this, "Movie" + movie.title + "\n Last Item?: " + isLastItem, Toast.LENGTH_SHORT).show();
    }

    private List<Movie> getMovies() {
        List<Movie> movies = new ArrayList<Movie>();
        movies.add(new Movie("1", "Squid Game (2021)", R.drawable.a));
        movies.add(new Movie("2", "Mortal Kombat (2021)", R.drawable.b));
        movies.add(new Movie("3", "PAW Patrol: The Movie (2021)", R.drawable.c));
        movies.add(new Movie("4", "Train to Busan (2016)", R.drawable.d));
        movies.add(new Movie("5", "The Walking Dead (2010)", R.drawable.e));
        return movies;
    }

}