package com.yomemo.clicklistenerexample;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private final List<Movie> movies;
    private OnClickListener onClickListener;

    public MovieAdapter(List<Movie> data) {
        this.movies = data;
    }

    public Movie getMovie(int position) {
        return movies.get(position);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public boolean isLastItem(int position) {
        return movies.size() - 1 == position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the indiv item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(view, this.onClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(this.movies.get(position).title);
        holder.image.setImageResource(this.movies.get(position).image);
    }

    @Override
    public int getItemCount() {
        return this.movies.size();
    }

    public interface OnClickListener {
        void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView title;
        private final ImageView image;
        private final LinearLayout itemView;
        private final OnClickListener onClickListener;

        public ViewHolder(View view, OnClickListener onClickListener) {
            super(view);
            this.title = view.findViewById(R.id.title);
            this.image = view.findViewById(R.id.image);
            this.itemView = view.findViewById(R.id.item_view);
            itemView.setOnClickListener(this);
            this.onClickListener = onClickListener;
        }

        @Override
        public void onClick(View v) {
            Log.d("", "getAdapterPosition: " + getAdapterPosition());
            int position = getAdapterPosition();
            this.onClickListener.onItemClick(position);
        }
    }
}